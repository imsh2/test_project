<?php

// This code works in PHP.
//
// Run builtin PHP web server like so:
//
//   php -S localhost:8080 ./main.php
//
// Then visit this page in your browser:
//
//   http://localhost:8080/?accounts[]=Rulon_User&accounts[]=Bidon_Admin
//
// The output should be:
//
//   name=Rulon
//   name=Bidon
// 
// TODO: Make this code compile and run with KPHP with minimal
// changes while trying to keep the code as idiomatic as possible.


class AccountFactory {
  /** 
  * @param string $name
  * @param string $account_type
  * @return Account
  */
  public static function newAccount($name, $account_type) {
    switch ($account_type) {
      case "Admin":
        return new Admin($name);
        break;
      case "User":
        return new User($name);
        break;
      default:
        throw new Exception('No such type');
    }
  }
}

interface Account {
  public function getName();
  public function setName($name);
  public function getValid();
  public function setValid($is_valid);
}

class Admin implements Account {
  /** @var string $name */
  private $name;
  /** @var bool $is_valid */
  private $is_valid;
  public function __construct($name) {
    $this->name = $name;
  }
  public function getName(){
    return $this->name;
  }
  public function setName($name){
    $this->name = $name;
  }
  public function getValid(){
    return $this->is_valid;
  }
  public function setValid($is_valid){
    $this->is_valid = $is_valid;
  }
}

class User implements Account {
  /** @var string $name */
  private $name;
  /** @var bool $is_valid */
  private $is_valid;
  public function __construct($name) {
    $this->name = $name;
  }
  public function getName(){
    return $this->name;
  }
  public function setName($name){
    $this->name = $name;
  }
  public function getValid(){
    return $this->is_valid;
  }
  public function setValid($is_valid){
    $this->is_valid = $is_valid;
  }
}

/** @param Account[] $accounts */
function validate_accounts(array &$accounts) {
  foreach ($accounts as $a) {
    $a->setValid(!empty($a->getName()));
  }
}

/** 
 * @param Account[] $accounts 
 * @return string[]
 */
function collect_names(array $accounts) {
  return array_map(function ($a) {
    return $a->getName();
  }, $accounts);
}

/** @param string[] $names */
function print_names($names) {
  foreach ($names as $name) {
    echo "name=$name<br>";
  }
}


function main() {
  $raw_accounts = $_GET['accounts'];
  if (!$raw_accounts) {
    die('$_GET["accounts"] is empty');
  }
  /** @var Account[] $accounts */
  $accounts = [];
  foreach ($raw_accounts as $raw) {
    $parts = explode('_', $raw);
    if (count($parts) !== 2) {
      continue;
    }
    try {
      $accounts[] = AccountFactory::newAccount($parts[0], $parts[1]);
    } catch (Exception $e) {
      echo 'Exception: ', $e->getMessage(), "\n";
    }
  }
  validate_accounts($accounts);
  $validated_accounts = array_filter($accounts, function ($a) {
    return $a->getValid();
  });
  $names = collect_names($validated_accounts);
  print_names($names);
}

main();

?>
